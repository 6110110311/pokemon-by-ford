import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Inventory extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;

	public Inventory (Trainer trainer){
		super("Your Inventory");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("inventory_1.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

		JButton pokedex = new JButton();
		JButton inv = new JButton();

		pokedex.setIcon(new ImageIcon("pokedex.png"));
	    inv.setIcon(new ImageIcon("bag.png"));

	    pokedex.setBounds(340, 220, 250, 250);
	    inv.setBounds(690, 220, 250, 250);

	    JButton back = new JButton("<<<");
        back.setBounds(20, 630, 80, 40);
        back.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

	    background.add(pokedex);
        background.add(inv);
        background.add(back);

        back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Mainplay mp = new Mainplay(trainer);
                setVisible(false);
            }
        });

        pokedex.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	Pokedex dex = new Pokedex(trainer);
                setVisible(false);
            }
        });

        inv.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	Backpacks b = new Backpacks(trainer);
                setVisible(false);
            }
        });


	}

}