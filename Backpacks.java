import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Backpacks extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;

	public Backpacks(Trainer trainer){
		super("Your Pokedex");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();


        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("backpacks.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

        JLabel bagpack;
        ImageIcon pack = new ImageIcon("bag.png");
        bagpack = new JLabel(pack);
        bagpack.setBounds(120, 630, 250, 250);

        JLabel pcoin = new JLabel("" + item.get_coin() + "", JLabel.CENTER);
        pcoin.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        pcoin.setBounds(150, 220, 100, 50);

        JLabel numberry = new JLabel("" + item.get_berry() + "");
		numberry.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numberry.setBounds(1150, 125, 40, 40);

        JLabel numpotion = new JLabel("" + item.get_potion() + "");
		numpotion.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numpotion.setBounds(1150, 270, 40, 40);

        JLabel numenergy = new JLabel("" + item.get_energy() + "");
		numenergy.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numenergy.setBounds(1150, 419, 40, 40);

        JLabel numpkball = new JLabel("" + item.get_pokeball() + "");
		numpkball.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numpkball.setBounds(1150, 555, 40, 40);

        JButton back = new JButton("<<<");
        back.setBounds(20, 630, 80, 40);
        back.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

		background.add(pcoin);
        background.add(numberry);
        background.add(numpotion);
        background.add(numenergy);
        background.add(numpkball);
        background.add(back);

        back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Inventory inv = new Inventory(trainer);
                setVisible(false);
            }
        });

	}

}