import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Maingame extends JFrame{
    private Trainer trainer;
    private ArrayList<Pokemon> bag;
    private Item item;
    private String name;
    private int no = 0;

    public Maingame(Trainer trainer){
        super("Pokemon Game");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
        this.name = trainer.getName();
        
        Container c = getContentPane();
        c.setLayout(new BorderLayout());

        JPanel head = new JPanel();
        head.setLayout(new GridLayout(3, 1, 0, 0));

        JLabel title = new JLabel("Please Select Your First Pokemon !", JLabel.CENTER);
        JLabel trainername = new JLabel("Welcome " + name + "", JLabel.CENTER);

        title.setFont(new Font("Courier", Font.BOLD, 36));
        trainername.setFont(new Font("Courier", Font.BOLD, 36));

        head.add(trainername);
        head.add(title);
        

        c.add(head, BorderLayout.NORTH);

        ImageIcon fd = new ImageIcon("Fuchidane_new.png");
        ImageIcon hk = new ImageIcon("Hitokage_new.png");
        ImageIcon sk = new ImageIcon("Sanikamea_new.png");

        JPanel fuchidane = new JPanel();
        JPanel hitokage = new JPanel();
        JPanel sanikamea = new JPanel();
        JPanel choose = new JPanel();
        JPanel pokemon = new JPanel();

        choose.setLayout(new GridLayout(3,1));
        pokemon.setLayout(new FlowLayout());

        fuchidane.setLayout(new FlowLayout());
        hitokage.setLayout(new FlowLayout());
        sanikamea.setLayout(new FlowLayout());

        JLabel imgfd = new JLabel(fd);
        JButton bt_fuchidane = new JButton("Fuchidane");
        bt_fuchidane.setFont(new Font("Courier", Font.BOLD, 20));
        bt_fuchidane.setBounds(500,500,200,50);
        fuchidane.add(imgfd);
        pokemon.add(bt_fuchidane);

        
        JLabel imghk = new JLabel(hk);
        JButton bt_hitokage = new JButton("Hitokage");
        bt_hitokage.setFont(new Font("Courier", Font.BOLD, 20));
        bt_hitokage.setBounds(550,500,200,50);
        hitokage.add(imghk);
        pokemon.add(bt_hitokage);

        

        JLabel imgsk = new JLabel(sk);
        JButton bt_sanikamea = new JButton("Sanikamea");
        bt_sanikamea.setFont(new Font("Courier", Font.BOLD, 20));
        bt_sanikamea.setBounds(600,500,200,50);
        sanikamea.add(imgsk);
        pokemon.add(bt_sanikamea);

        c.add(hitokage, BorderLayout.CENTER);
        c.add(sanikamea, BorderLayout.EAST);
        c.add(fuchidane, BorderLayout.WEST);    

        JButton ok = new JButton("OK");
        choose.add(pokemon);
        choose.add(ok);

        c.add(choose,BorderLayout.SOUTH);


        bt_fuchidane.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 1;
                System.out.println("1");
            }
        });

        bt_hitokage.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 2;
                System.out.println("2");
            }
        });

        bt_sanikamea.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 3;
                System.out.println("3");
            }
        });

        ok.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("OK");
                if(no == 1){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        bag.add(new Fuchidane(namepk));
                        trainer.set_Bag(bag);
                        setVisible(false);
                        System.out.println("fuchidane");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
                else if(no == 2){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        bag.add(new Hitokage(namepk));
                        trainer.set_Bag(bag);
                        setVisible(false);
                        System.out.println("hitokage");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
                else if(no == 3){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        bag.add(new Sanikamea(namepk));
                        trainer.set_Bag(bag);
                        setVisible(false);
                        System.out.println("sanikamea");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
            }
        });    
    }

}