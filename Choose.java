import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Choose extends JFrame{
    private Trainer trainer;
    private ArrayList<Pokemon> bag;
    private Item item;
    private String name;
    private int no = 1;

    public Choose(Trainer trainer){
        super("Choose Your Pokemon");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
        this.name = trainer.getName();

        Pokemon poke1 = new Fuchidane("");
        Pokemon poke2 = new Hitokage("");
        Pokemon poke3 = new Sanikamea("");

        ArrayList<Skill> skills_f = poke1.getskill();
        ArrayList<Skill> skills_h = poke2.getskill();
        ArrayList<Skill> skills_s = poke3.getskill();

        
        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("choosein.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

        JLabel title = new JLabel("Please Select Your First Pokemon !", JLabel.CENTER);
        title.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        title.setBounds(225, 30, 800, 50);

        JLabel trainername = new JLabel("Welcome " + name + " !!", JLabel.CENTER);
        trainername.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        trainername.setBounds(400, 620, 350, 50);

        ImageIcon fd = new ImageIcon("Fuchidane_new.png");
        ImageIcon hk = new ImageIcon("Hitokage_new.png");
        ImageIcon sk = new ImageIcon("Sanikamea_new.png");

        JLabel pokemon = new JLabel(fd);
        pokemon.setBounds(160, 150, 250, 320);

        JButton fuchidane = new JButton("Fuchidane");
        fuchidane.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        fuchidane.setBounds(50,500,150,40);

        JButton hitokage = new JButton("Hitokage");
        hitokage.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        hitokage.setBounds(210,500,150,40);

        JButton sanikamea = new JButton("Sanikamea");
        sanikamea.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        sanikamea.setBounds(370,500,150,40);

        // System.out.println("Skill 1:\t " + skills_f.get(0).get_name() + " \tDamage: " + skills_f.get(0).get_damage() + " MP: " + skills_f.get(0).get_Mp());
        // System.out.println("Skill 2:\t " + skills_f.get(1).get_name() + " \tDamage: " + skills_f.get(1).get_damage() + " MP: " + skills_f.get(1).get_Mp());
        // System.out.println("Skill 3:\t " + skills_f.get(2).get_name() + " \tDamage: " + skills_f.get(2).get_damage() + " MP: " + skills_f.get(2).get_Mp());
        // System.out.println("Skill 4:\t " + skills_f.get(3).get_name() + " \tDamage: " + skills_f.get(3).get_damage() + " MP: " + skills_f.get(3).get_Mp());
        
        JLabel skill_1 = new JLabel("Skill 1:\t " + skills_f.get(0).get_name() + " \tDamage: " + skills_f.get(0).get_damage() + " \tMP: " + skills_f.get(0).get_Mp());
        skill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        skill_1.setBounds(700,150,600,40);

        JLabel skill_2 = new JLabel("Skill 2:\t " + skills_f.get(1).get_name() + " \tDamage: " + skills_f.get(1).get_damage() + " \tMP: " + skills_f.get(1).get_Mp());
        skill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        skill_2.setBounds(700,225,600,40);

        JLabel skill_3 = new JLabel("Skill 3:\t " + skills_f.get(2).get_name() + " \tDamage: " + skills_f.get(2).get_damage() + " \tMP: " + skills_f.get(2).get_Mp());
        skill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        skill_3.setBounds(700,300,600,40);

        JLabel skill_4 = new JLabel("Skill 4:\t " + skills_f.get(3).get_name() + " \tDamage: " + skills_f.get(3).get_damage() + " \tMP: " + skills_f.get(3).get_Mp());
        skill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        skill_4.setBounds(700,375,600,40);

        JButton next = new JButton("Next >>");
        next.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        next.setBounds(1100,630,150,40);

        background.add(title);
        background.add(trainername);
        background.add(pokemon);
        background.add(fuchidane);
        background.add(skill_1);
        background.add(skill_2);
        background.add(skill_3);
        background.add(skill_4);
        background.add(hitokage);
        background.add(sanikamea);
        background.add(next);

        fuchidane.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 1;
                // System.out.println("1");
                pokemon.setIcon(fd);
                skill_1.setText("Skill 1:\t " + skills_f.get(0).get_name() + " \tDamage: " + skills_f.get(0).get_damage() + " \tMP: " + skills_f.get(0).get_Mp());
                skill_2.setText("Skill 2:\t " + skills_f.get(1).get_name() + " \tDamage: " + skills_f.get(1).get_damage() + " \tMP: " + skills_f.get(1).get_Mp());
                skill_3.setText("Skill 3:\t " + skills_f.get(2).get_name() + " \tDamage: " + skills_f.get(2).get_damage() + " \tMP: " + skills_f.get(2).get_Mp());
                skill_4.setText("Skill 4:\t " + skills_f.get(3).get_name() + " \tDamage: " + skills_f.get(3).get_damage() + " \tMP: " + skills_f.get(3).get_Mp());
            }
        });

        hitokage.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 2;
                // System.out.println("2");
                pokemon.setIcon(hk);
                skill_1.setText("Skill 1:\t " + skills_h.get(0).get_name() + " \tDamage: " + skills_h.get(0).get_damage() + " \tMP: " + skills_h.get(0).get_Mp());
                skill_2.setText("Skill 2:\t " + skills_h.get(1).get_name() + " \tDamage: " + skills_h.get(1).get_damage() + " \tMP: " + skills_h.get(1).get_Mp());
                skill_3.setText("Skill 3:\t " + skills_h.get(2).get_name() + " \tDamage: " + skills_h.get(2).get_damage() + " \tMP: " + skills_h.get(2).get_Mp());
                skill_4.setText("Skill 4:\t " + skills_h.get(3).get_name() + " \tDamage: " + skills_h.get(3).get_damage() + " \tMP: " + skills_h.get(3).get_Mp());
            }
        });

        sanikamea.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                no = 3;
                // System.out.println("3");
                pokemon.setIcon(sk);
                skill_1.setText("Skill 1:\t " + skills_s.get(0).get_name() + " \tDamage: " + skills_s.get(0).get_damage() + " \tMP: " + skills_s.get(0).get_Mp());
                skill_2.setText("Skill 2:\t " + skills_s.get(1).get_name() + " \tDamage: " + skills_s.get(1).get_damage() + " \tMP: " + skills_s.get(1).get_Mp());
                skill_3.setText("Skill 3:\t " + skills_s.get(2).get_name() + " \tDamage: " + skills_s.get(2).get_damage() + " \tMP: " + skills_s.get(2).get_Mp());
                skill_4.setText("Skill 4:\t " + skills_s.get(3).get_name() + " \tDamage: " + skills_s.get(3).get_damage() + " \tMP: " + skills_s.get(3).get_Mp());
            }
        });

        next.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                // System.out.println("next");
                if(no == 1){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        poke1.new_name(namepk);
                        bag.add(poke1);
                        trainer.set_Bag(bag);
                        setVisible(false);
                        // System.out.println("fuchidane");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
                else if(no == 2){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        poke2.new_name(namepk);
                        bag.add(poke2);
                        trainer.set_Bag(bag);
                        setVisible(false);
                        // System.out.println("hitokage");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
                else if(no == 3){
                    String namepk = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepk != null){
                        poke3.new_name(namepk);
                        bag.add(poke3);
                        trainer.set_item(item);
                        trainer.set_Bag(bag);
                        setVisible(false);
                        // System.out.println("sanikamea");
                        Mainplay mp = new Mainplay(trainer);
                    }
                }
            }
        }); 
    }
}