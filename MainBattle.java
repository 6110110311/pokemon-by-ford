import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MainBattle extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private ArrayList<Pokemon> bag_new = new ArrayList<Pokemon>();
	private Item item;
	private int no = 5;
	private int number;
	private Pokemon myPokemon;
	private Pokemon wildPokemon;
	private int status = 0;

	public MainBattle(Trainer trainer, int no, Pokemon wildPokemon){
		super("Your Pokedex");
        setVisible(true);
        setSize(1280, 740);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
        this.myPokemon = bag.get(no);
        this.wildPokemon = wildPokemon;

        ArrayList<Skill> skills = myPokemon.getskill();
        ArrayList<Skill> skillsbot = wildPokemon.getskill();

        ImageIcon qs = new ImageIcon("question.png");
        ImageIcon fd = new ImageIcon("dex_fuchidane.png");
        ImageIcon hk = new ImageIcon("dex_hitokage.png");
        ImageIcon kb = new ImageIcon("dex_kabigon.png");
        ImageIcon pi = new ImageIcon("dex_pikachu.png");
        ImageIcon sk = new ImageIcon("dex_sanikamea.png");


        ImageIcon myfd = new ImageIcon("fuchidane_new.png");
        ImageIcon myhk = new ImageIcon("Hitokage_new.png");
        ImageIcon mykb = new ImageIcon("dex_kabigon.png");
        ImageIcon mypi = new ImageIcon("dex_pikachu.png");
        ImageIcon mysk = new ImageIcon("Sanikamea_new.png");

        // System.out.println(myPokemon);
        // System.out.println(wildPokemon);

        Container c = getContentPane();
		JLabel background;
        ImageIcon bg = new ImageIcon("arena.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);


        JLabel wildname = new JLabel("Name: " + wildPokemon.getName());
        wildname.setBounds(425, 65, 500, 50);
        wildname.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 35));
        JLabel wildhp = new JLabel("HP: " + wildPokemon.get_hp() + " / " + wildPokemon.get_maxhp());
        wildhp.setBounds(430, 105, 250, 50);
        wildhp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 25));
        JLabel wildmp = new JLabel("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
        wildmp.setBounds(580, 105, 250, 50);
        wildmp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 25));
        JLabel wild = new JLabel(qs);
        wild.setBounds(875, 100, 210, 190);

		if(wildPokemon.get_type() == 1){
    		wild.setIcon(fd);
    	}
    	else if(wildPokemon.get_type() == 2){
    		wild.setIcon(hk);
    	}
    	else if(wildPokemon.get_type() == 3){
    		wild.setIcon(kb);
    	}
    	else if(wildPokemon.get_type() == 4){
    		wild.setIcon(pi);
    	}
    	else if(wildPokemon.get_type() == 5){
    		wild.setIcon(sk);
    	}




        JLabel myname = new JLabel("Name: " + myPokemon.getName());
        myname.setBounds(780, 380, 500, 60);
        myname.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 60));
        JLabel myhp = new JLabel("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
        myhp.setBounds(785, 450, 250, 50);
        myhp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        JLabel mymp = new JLabel("MP: " + myPokemon.get_mp() + " / " + myPokemon.get_maxmp());
        mymp.setBounds(1000, 450, 250, 50);
        mymp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        JLabel my = new JLabel(qs);
        my.setBounds(230, 180, 250, 320);

        if(myPokemon.get_type() == 1){
    		my.setIcon(myfd);
    	}
    	else if(myPokemon.get_type() == 2){
    		my.setIcon(myhk);
    	}
    	else if(myPokemon.get_type() == 3){
    		my.setIcon(mykb);
    	}
    	else if(myPokemon.get_type() == 4){
    		my.setIcon(mypi);
    	}
    	else if(myPokemon.get_type() == 5){
    		my.setIcon(mysk);
    	}



        JButton skill_1 = new JButton("" + skills.get(0).get_name() + " DM: " + skills.get(0).get_damage() + " MP: " + skills.get(0).get_Mp());
        skill_1.setBounds(15, 532, 445, 76);
        skill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton skill_2 = new JButton("" + skills.get(1).get_name() + " DM: " + skills.get(1).get_damage() + " MP: " + skills.get(1).get_Mp());
        skill_2.setBounds(460, 532, 445, 76);
        skill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton skill_3 = new JButton("" + skills.get(2).get_name() + " DM: " + skills.get(2).get_damage() + " MP: " + skills.get(2).get_Mp());
        skill_3.setBounds(15, 608, 445, 76);
        skill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton skill_4 = new JButton("" + skills.get(3).get_name() + " DM: " + skills.get(3).get_damage() + " MP: " + skills.get(3).get_Mp());
        skill_4.setBounds(460, 608, 445, 76);
        skill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));


		JButton ct = new JButton("Catch");
        ct.setBounds(958, 532, 147, 76);
        ct.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton item = new JButton("Item");
        item.setBounds(958, 608, 147, 76);
        item.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton change = new JButton("Change");
        change.setBounds(1105, 532, 145, 76);
        change.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton run = new JButton("Run");
        run.setBounds(1105, 608, 145, 76);
        run.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        
        background.add(wild);
        background.add(wildname);
        background.add(wildhp);
        background.add(wildmp);

        background.add(my);
        background.add(myname);
        background.add(myhp);
        background.add(mymp);

        background.add(skill_1);
        background.add(skill_2);
        background.add(skill_3);
        background.add(skill_4);

        background.add(ct);
        background.add(item);
        background.add(change);
        background.add(run);

        item.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	setVisible(false);
                Useitem ui = new Useitem(trainer, no, wildPokemon);
                
            }
        });

        change.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(bag.size() > 1){
                	Change ch = new Change(trainer, no, wildPokemon);
                	setVisible(false);
                }
                else{
                	JOptionPane.showMessageDialog(null, "Not Other Pokemon !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                	setVisible(true);
                }
                
            }
        });

        ct.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(trainer.get_Bag().size() < 5){
	            	int ballnum = trainer.get_item().get_pokeball();
	            	if(ballnum > 0){
	            		trainer.get_item().set_pokeball();
	            		if(wildPokemon.get_hp() < (wildPokemon.get_maxhp() / 2)){
	            			int random = (int)(Math.random()* 2) + 1;
	            			if(random == 1){
				                System.out.println("-------------------------------");
				                System.out.println("Catched it Sucess !!");
				                System.out.println("-------------------------------");
				                JOptionPane.showMessageDialog(null, "Catch it!", "!!!!!", JOptionPane.PLAIN_MESSAGE);
				                trainer.get_Bag().add(wildPokemon);
				                Mainplay mg = new Mainplay(trainer);
				                System.out.println(wildPokemon);
				                setVisible(false);
				            }
				            else if (random == 2){
				                System.out.println("-------------------------------");
				                System.out.println("Catched it Fail !!");
				                System.out.println("-------------------------------");
				            }
						}
			        	else{
				            System.out.println("-------------------------------");
				            System.out.println("Catched it Fail !!");
				            System.out.println("-------------------------------");
			        	}	
	            	}
	            	else{
	            		System.out.println("Not ball");
	            	}
            	}
            	else{
            		JOptionPane.showMessageDialog(null, "Your Bag is Full !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
            	}
            }
        });

        run.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Mainplay mg = new Mainplay(trainer);
                setVisible(false);
            }
        });
        
        skill_1.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(myPokemon.get_hp() > 0){
	            	if(myPokemon.get_mp() >= skills.get(0).get_Mp()){
	            		myPokemon.skill_1(wildPokemon, myPokemon, skills.get(0));
	            		mymp.setText("MP: " + myPokemon.get_mp() + " / " + myPokemon.get_maxmp());
	            		wildhp.setText("HP: " + wildPokemon.get_hp() + " / " + wildPokemon.get_maxhp());
	            		if(wildPokemon.get_hp() == 0){
	            			Win win = new Win(trainer);
	            			setVisible(false);
	            		}
	            	}
	            	else{
	            		JOptionPane.showMessageDialog(null, "Low MP Plaese use MP Potion !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            	} 	
				}

				if(wildPokemon.get_hp() > 0){
					int bot = (int)(Math.random() * 4) + 1;
	            	if(bot == 1){
	            		if(wildPokemon.get_mp() >= skillsbot.get(0).get_Mp()){
	            			wildPokemon.skill_1(myPokemon, wildPokemon, skillsbot.get(0));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 2){
	            		if(wildPokemon.get_mp() >= skillsbot.get(1).get_Mp()){
	            			wildPokemon.skill_2(myPokemon, wildPokemon, skillsbot.get(1));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 3){
	            		if(wildPokemon.get_mp() >= skillsbot.get(2).get_Mp()){
	            			wildPokemon.skill_3(myPokemon, wildPokemon, skillsbot.get(2));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 4){
	            		if(wildPokemon.get_mp() >= skillsbot.get(3).get_Mp()){
	            			wildPokemon.skill_4(myPokemon, wildPokemon, skillsbot.get(3));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
				}
           
            }
        });

        skill_2.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(myPokemon.get_hp() > 0){
	            	if(myPokemon.get_mp() >= skills.get(1).get_Mp()){
	            		myPokemon.skill_1(wildPokemon, myPokemon, skills.get(1));
	            		mymp.setText("MP: " + myPokemon.get_mp() + " / " + myPokemon.get_maxmp());
	            		wildhp.setText("HP: " + wildPokemon.get_hp() + " / " + wildPokemon.get_maxhp());
	            		if(wildPokemon.get_hp() == 0){
	            			Win win = new Win(trainer);
	            			setVisible(false);
	            		}
	            	}
	            	else{
	            		JOptionPane.showMessageDialog(null, "Low MP Plaese use MP Potion !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            	}

	            	
				}
				if(wildPokemon.get_hp() > 0){
					int bot = (int)(Math.random() * 4) + 1;
	            	if(bot == 1){
	            		if(wildPokemon.get_mp() >= skillsbot.get(0).get_Mp()){
	            			wildPokemon.skill_1(myPokemon, wildPokemon, skillsbot.get(0));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 2){
	            		if(wildPokemon.get_mp() >= skillsbot.get(1).get_Mp()){
	            			wildPokemon.skill_2(myPokemon, wildPokemon, skillsbot.get(1));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 3){
	            		if(wildPokemon.get_mp() >= skillsbot.get(2).get_Mp()){
	            			wildPokemon.skill_3(myPokemon, wildPokemon, skillsbot.get(2));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 4){
	            		if(wildPokemon.get_mp() >= skillsbot.get(3).get_Mp()){
	            			wildPokemon.skill_4(myPokemon, wildPokemon, skillsbot.get(3));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
				}
            }
        });

        skill_3.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(myPokemon.get_hp() > 0){
	            	if(myPokemon.get_mp() >= skills.get(3).get_Mp()){
	            		myPokemon.skill_1(wildPokemon, myPokemon, skills.get(3));
	            		mymp.setText("MP: " + myPokemon.get_mp() + " / " + myPokemon.get_maxmp());
	            		wildhp.setText("HP: " + wildPokemon.get_hp() + " / " + wildPokemon.get_maxhp());
	            		if(wildPokemon.get_hp() == 0){
	            			Win win = new Win(trainer);
	            			setVisible(false);
	            		}
	            	}
	            	else{
	            		JOptionPane.showMessageDialog(null, "Low MP Plaese use MP Potion !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            	}

	            	
				}
				if(wildPokemon.get_hp() > 0){
					int bot = (int)(Math.random() * 4) + 1;
	            	if(bot == 1){
	            		if(wildPokemon.get_mp() >= skillsbot.get(0).get_Mp()){
	            			wildPokemon.skill_1(myPokemon, wildPokemon, skillsbot.get(0));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 2){
	            		if(wildPokemon.get_mp() >= skillsbot.get(1).get_Mp()){
	            			wildPokemon.skill_2(myPokemon, wildPokemon, skillsbot.get(1));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 3){
	            		if(wildPokemon.get_mp() >= skillsbot.get(2).get_Mp()){
	            			wildPokemon.skill_3(myPokemon, wildPokemon, skillsbot.get(2));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 4){
	            		if(wildPokemon.get_mp() >= skillsbot.get(3).get_Mp()){
	            			wildPokemon.skill_4(myPokemon, wildPokemon, skillsbot.get(3));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            }
			}
        });

        skill_4.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(myPokemon.get_hp() > 0){
	            	if(myPokemon.get_mp() >= skills.get(3).get_Mp()){
	            		myPokemon.skill_1(wildPokemon, myPokemon, skills.get(3));
	            		mymp.setText("MP: " + myPokemon.get_mp() + " / " + myPokemon.get_maxmp());
	            		wildhp.setText("HP: " + wildPokemon.get_hp() + " / " + wildPokemon.get_maxhp());
	            		if(wildPokemon.get_hp() == 0){
	            			Win win = new Win(trainer);
	            			setVisible(false);
	            		}
	            	}
	            	else{
	            		JOptionPane.showMessageDialog(null, "Low MP Plaese use MP Potion !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            	}

	            	
				}
				if(wildPokemon.get_hp() > 0){
					int bot = (int)(Math.random() * 4) + 1;
	            	if(bot == 1){
	            		if(wildPokemon.get_mp() >= skillsbot.get(0).get_Mp()){
	            			wildPokemon.skill_1(myPokemon, wildPokemon, skillsbot.get(0));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 2){
	            		if(wildPokemon.get_mp() >= skillsbot.get(1).get_Mp()){
	            			wildPokemon.skill_2(myPokemon, wildPokemon, skillsbot.get(1));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 3){
	            		if(wildPokemon.get_mp() >= skillsbot.get(2).get_Mp()){
	            			wildPokemon.skill_3(myPokemon, wildPokemon, skillsbot.get(2));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
	            	else if(bot == 4){
	            		if(wildPokemon.get_mp() >= skillsbot.get(3).get_Mp()){
	            			wildPokemon.skill_4(myPokemon, wildPokemon, skillsbot.get(3));
	            			myhp.setText("HP: " + myPokemon.get_hp() + " / " + myPokemon.get_maxhp());
	            			wildmp.setText("MP: " + wildPokemon.get_mp() + " / " + wildPokemon.get_maxmp());
	            			if(myPokemon.get_hp() == 0){
	            				Lose lose = new Lose(trainer);
	            				setVisible(false);
	            			}
	            		}
	            		else{
	            			JOptionPane.showMessageDialog(null, "Enermy Can't Attack You !", "!!!!!", JOptionPane.PLAIN_MESSAGE);
	            		}
	            	}
				}
            }
        });

		

	}

}