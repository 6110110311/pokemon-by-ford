import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Shop extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;
    private String number;
    private int num;

	public Shop(Trainer trainer){
		super("Pokemon Shop");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();

        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("shopin.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

        JButton back = new JButton("<<<");
        back.setBounds(20, 630, 80, 40);
        back.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

        JLabel pkcoin;
        ImageIcon coin = new ImageIcon("pkcoin.png");
        pkcoin = new JLabel(coin);
        pkcoin.setBounds(1200, 630, 50, 50);
        JLabel pcoin = new JLabel("" + item.get_coin() + "", JLabel.CENTER);
        pcoin.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        pcoin.setBounds(1100, 630, 100, 50);


        JLabel itembag;
        ImageIcon itemc = new ImageIcon("iteminshop.png");
        itembag = new JLabel(itemc);
        itembag.setBounds(20, 20, 250, 600);

        JLabel numberry = new JLabel("" + item.get_berry() + "");
		numberry.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numberry.setBounds(180, 225, 40, 40);
        JLabel numpotion = new JLabel("" + item.get_potion() + "");
		numpotion.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numpotion.setBounds(180, 330, 40, 40);
        JLabel numenergy = new JLabel("" + item.get_energy() + "");
		numenergy.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numenergy.setBounds(180, 440, 40, 40);
        JLabel numpkball = new JLabel("" + item.get_pokeball() + "");
		numpkball.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        numpkball.setBounds(180, 540, 40, 40);


        JLabel title = new JLabel("Welcome To PokeShop !!", JLabel.CENTER);
        title.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        title.setBounds(510, 20, 500, 50);



		JButton berry = new JButton();
		berry.setIcon(new ImageIcon("berry.png"));
		berry.setBounds(500, 150, 90, 90);
		JLabel pberry = new JLabel("1 Berry : 15 Pokecoin");
		pberry.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 20));
        pberry.setBounds(470, 230, 200, 50);

        JButton pkball = new JButton();
        pkball.setIcon(new ImageIcon("pokeball.png"));
        pkball.setBounds(500, 300 , 90, 90);
		JLabel ppkball = new JLabel("1 Pokemball : 30 Pokecoin");
		ppkball.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 20));
        ppkball.setBounds(470, 380, 200, 50);



        JButton potion = new JButton();
        potion.setIcon(new ImageIcon("potion.png"));
        potion.setBounds(880, 150 , 90, 90);
		JLabel ppotion = new JLabel("1 Potion : 25 Pokecoin");
		ppotion.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 20));
        ppotion.setBounds(850, 230, 200, 50);

        JButton energy = new JButton();
        energy.setIcon(new ImageIcon("energy.png"));
        energy.setBounds(880, 300 , 90, 90);
		JLabel penergy = new JLabel("1 Energy : 20 Pokecoin");
		penergy.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 20));
        penergy.setBounds(850, 380, 200, 50);



        JButton heal = new JButton();
        heal.setIcon(new ImageIcon("heal.png"));
        heal.setBounds(685, 450 , 100, 140);
		JLabel pheal = new JLabel("Heal All Pokemons : 50 Pokecoin");
		pheal.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 20));
        pheal.setBounds(620, 580, 300, 50);



        

        background.add(title);
        background.add(back);
        background.add(itembag);

        itembag.add(numberry);
        itembag.add(numpotion);
        itembag.add(numenergy);
        itembag.add(numpkball);

        background.add(berry);
        background.add(pberry);
        background.add(pkcoin);
        background.add(pcoin);
        background.add(potion);
        background.add(ppotion);
        background.add(pkball);
        background.add(ppkball);
        background.add(energy);
        background.add(penergy);
        background.add(heal);
        background.add(pheal);

        

		back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Mainplay mp = new Mainplay(trainer);
                setVisible(false);
            }
        });

		berry.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                number = JOptionPane.showInputDialog(null, "Enter Your Number:", "SET NUM", JOptionPane.PLAIN_MESSAGE);
                try{
                	num = Integer.parseInt(number);
                	if(num > 0){
                		if(item.get_coin() >= (num*15)){
                			item.add_berry(num, 15);
                            pcoin.setText("" + item.get_coin() + "");
                            numberry.setText("" + item.get_berry() + "");
                			// System.out.print("Done !! ");
                		}
                		else{
                			JOptionPane.showMessageDialog(null, "Not Have Pokecoin", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                		}
                	}
                	else{
                		JOptionPane.showMessageDialog(null, "Out of Command", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                	}
                }catch(NumberFormatException f){
                    JOptionPane.showMessageDialog(null, "Please Enter Number", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });

        pkball.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                number = JOptionPane.showInputDialog(null, "Enter Your Number:", "SET NUM", JOptionPane.PLAIN_MESSAGE);
                try{
                    num = Integer.parseInt(number);
                    if(num > 0){
                        if(item.get_coin() >= (num*30)){
                            item.add_pokeball(num, 30);
                            pcoin.setText("" + item.get_coin() + "");
                            numpkball.setText("" + item.get_pokeball() + "");
                            // System.out.print("Done !! ");
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "Not Have Pokecoin", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Out of Command", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                    } 
                }catch(NumberFormatException f){
                    JOptionPane.showMessageDialog(null, "Please Enter Number", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });

        potion.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                number = JOptionPane.showInputDialog(null, "Enter Your Number:", "SET NUM", JOptionPane.PLAIN_MESSAGE);
                
                try{
                    num = Integer.parseInt(number);
                    if(num > 0){
                        if(item.get_coin() >= (num*25)){
                            item.add_potion(num, 25);
                            pcoin.setText("" + item.get_coin() + "");
                            numpotion.setText("" + item.get_potion() + "");
                            // System.out.print("Done !! ");
                            }
                        else{
                            JOptionPane.showMessageDialog(null, "Not Have Pokecoin", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Out of Command", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                    } 
                }catch(NumberFormatException f){
                    JOptionPane.showMessageDialog(null, "Please Enter Number", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
                
            }
        });

        energy.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                number = JOptionPane.showInputDialog(null, "Enter Your Number:", "SET NUM", JOptionPane.PLAIN_MESSAGE);
                try{
                    
                    num = Integer.parseInt(number);
                    if(num > 0){
                        if(item.get_coin() >= (num*20)){
                            item.add_energy(num, 20);
                            pcoin.setText("" + item.get_coin() + "");
                            numenergy.setText("" + item.get_energy() + "");
                            // System.out.print("Done !! ");
                        }
                        else{
                                JOptionPane.showMessageDialog(null, "Not Have Pokecoin", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Out of Command", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                    }

                }catch(NumberFormatException f){
                    JOptionPane.showMessageDialog(null, "Please Enter Number", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
                
            }
        });


        heal.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(item.get_coin() >= 50){
                    item.pay_coin(50);
                    pcoin.setText("" + item.get_coin() + "");
                    for(Pokemon p: bag){
                        p.full_hp();
                    }
                    // System.out.print("Done to Heal !! ");
                }
                else{
                    JOptionPane.showMessageDialog(null, "Not Have Pokecoin", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });

	}
}