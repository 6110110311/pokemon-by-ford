import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PokemonGame extends JFrame{
    public PokemonGame(){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();
                
        JLabel background;
        ImageIcon bg = new ImageIcon("bg.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        JButton start = new JButton("START !!!");
        start.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        start.setBounds(550,650,180,40);
        
        background.add(start);
        start.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Enter Your Name:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(name != null){
                        setVisible(false);
                        Trainer t = new Trainer(name);
                        Choose mg = new Choose(t);
                    }
            }
        });
        

    }

    public static void main(String[] args) {
        new PokemonGame();
    }

}