import java.util.*;

public class Print{

    public void play(ArrayList<Pokemon> pokemons) {
        int number = 1;
        for(Pokemon p: pokemons){
            System.out.println(number + ". " + p + " HP: " + p.get_hp() + " MP : " + p.get_mp());
            number++;
        }
    }
}