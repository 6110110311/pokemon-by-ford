import java.util.*;

public class Trainer{
    private ArrayList<Pokemon> bag;
    private Scanner sc;
    private String name;
    private Item item = new Item();
    
    public Trainer(String name){
        bag = new ArrayList<Pokemon>();
        // bag.add(new Pikachu("Test"));
        sc = new Scanner(System.in);

        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void play(){
        int cmd = 0;
        int no = 0;
        String name_trainer = "";
        String nameyourPokemon = "";
        int firstPokemon = 0;
        boolean begin = true;
        boolean step1 = true;
        Print printPokemon = new Print();
        Battle bt = new Battle();
        
        System.out.print("\n\nPlease Enter Your Name : ");
        name_trainer = sc.nextLine();
        System.out.println("\n\nWelcome to Pokemon " + name_trainer + " mater");

        
            System.out.println("\nPlease Select Your First Pokemon !");

            while(step1){
                System.out.println("No.1 Fuchidane : Grass");
                System.out.println("No.2 Hitokage  : Fire");
                System.out.println("No.3 Sanikamea : Water");
                System.out.print("\n\nPlease Enter Number of Pokemon : ");
                firstPokemon = sc.nextInt();
                sc.nextLine();
                if(firstPokemon >= 1 && firstPokemon <= 3){
                    step1 = false;
                }else{
                    System.out.println("Please Try again!");
                }
            }

        System.out.print("Please Enter Your Pokemon Name : ");
        nameyourPokemon = sc.nextLine();

        if(firstPokemon == 1){
            bag.add(new Fuchidane(nameyourPokemon));
            System.out.println(nameyourPokemon +" is in the your bag!!");
        }
        else if(firstPokemon == 2){
            bag.add(new Hitokage(nameyourPokemon));
            System.out.println(nameyourPokemon +" is in the your bag!!");
        }
        else if(firstPokemon == 3){
            bag.add(new Sanikamea(nameyourPokemon));
            System.out.println(nameyourPokemon +" is in the your bag!!");
        }
        else{
            System.out.println("Please Try again!");
        }

        do{
            System.out.println("\n\n----------- Enter Your comamnd -------------");
            System.out.println("1. Pokedex  : To Show Your Pokemon in Your Bag");
            System.out.println("2. Battle   : To Fight Pokomen");
            System.out.println("3. Shop     : To Buy Item");
            System.out.println("4. Quit     : To End The Game!");
            System.out.print(">>> ");
            cmd = sc.nextInt();

            if(cmd == 1){
                System.out.println("1. Print      : To Show Your Pokemon in Your Bag");
                System.out.println("2. Rename     : To Change name of pokemon!");
                System.out.print(">>> ");
                cmd = sc.nextInt();
                if(cmd == 1){
                    System.out.println("\n\nPokemon in bag: ");
                    printPokemon.play(bag);
                }
                else if(cmd == 2){
                    System.out.print("\nSelect Your Pokemon in bag: ");
                    no = sc.nextInt();
                    Pokemon myPokemon = bag.get(no-1);
                    System.out.print("\nPlease Enter Your Pokemon Name : ");
                    nameyourPokemon = sc.nextLine();
                    myPokemon.new_name(nameyourPokemon);
                    
                }

            }
            else if(cmd == 2){ 
                System.out.println("Go Fight !!");
                bt.play(bag, item);
            }
            else if(cmd == 3){ 
                System.out.println("Go Shopping !!");
                System.out.println("\n\n----------- Choose Your Item -------------");
                System.out.println("1. Berry     1 piece : 15 Pokecoin");
                System.out.println("2. Potion    1 piece : 25 Pokecoin");
                System.out.println("3. Energy    1 piece : 20 Pokecoin");
                System.out.println("4. Pokeball  1 piece : 30 Pokecoin");
                System.out.println("\nPokecoin: " + item.get_coin());
                System.out.print(">>> ");
                int sp = sc.nextInt();
                if(sp == 1){
                    System.out.print("Enter Piece of Berry: ");
                    sp = sc.nextInt();
                    if(item.get_coin() >= (sp*15)){
                        item.add_berry(sp, 15);
                        System.out.print("Done !! ");
                    }
                    else{
                        System.out.print("Not Have Pokecoin !! ");
                    }
                }
                else if(sp == 2){
                    System.out.print("Enter Piece of Potion: ");
                    sp = sc.nextInt();
                    if(item.get_coin() >= (sp*25)){
                        item.add_potion(sp, 25);
                        System.out.print("Done !! ");
                    }
                    else{
                        System.out.print("Not Have Pokecoin !! ");
                    }
                }
                else if(sp == 3){
                    System.out.print("Enter Piece of Energy: ");
                    sp = sc.nextInt();
                    if(item.get_coin() >= (sp*20)){
                        item.add_energy(sp, 20);
                        System.out.print("Done !! ");
                    }
                    else{
                        System.out.print("Not Have Pokecoin !! ");
                    }
                }
                else if(sp == 4){
                    System.out.print("Enter Piece of Pokeball: ");
                    sp = sc.nextInt();
                    if(item.get_coin() >= (sp*30)){
                        item.add_pokeball(sp, 30);
                        System.out.print("Done !! ");
                    }
                    else{
                        System.out.print("Not Have Pokecoin !! ");
                    }
                }
                else{
                    System.out.print("Not Command");
                }
            }
        }while(!(cmd == 4));
    }

    public ArrayList<Pokemon> get_Bag(){
        return bag;
    }

    public void set_item(Item item){
        this.item = item;
    }

    public Item get_item(){
        return item;
    }

    public void set_Bag(ArrayList<Pokemon> bag){
        this.bag = bag;
    }
}