import java.util.*;

public class Skill{
    private String Nameskill = "";
    private String element = "";
    private int damage = 0;
    private int Mp = 0;
    
    public Skill(){
        
    }

    public Skill(String Nameskill, String element, int damage, int Mp){
        this.Nameskill = Nameskill;
        this.element = element;
        this.damage = damage;
        this.Mp = Mp;
    }

    public String get_name(){
        return Nameskill;
    }

    public int get_damage(){
        return damage;
    }
    
    public int get_Mp(){
        return Mp;
    }

}