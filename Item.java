import java.util.*;

public class Item{
    private int pokecoin = 200;
    private int berry = 5;
    private int potion = 5;
    private int pokeball = 5;
    private int energy = 5;

    public int get_coin(){
        return this.pokecoin;
    }

    public void add_coin(int money){
        this.pokecoin = this.pokecoin + money;
    }

    public void pay_coin(int money){
        this.pokecoin = this.pokecoin - money;
    }

    public int get_berry(){
        return this.berry;
    }

    public void add_berry(int num, int money){
        this.berry = this.berry + num;
        this.pokecoin = this.pokecoin - (money*num);
    }

    public void use_berry(Pokemon p){
        this.berry = this.berry - 1;
        p.heal_hp(20);
        System.out.println(" + 20 HP => Now HP is " + p.get_hp() + " ,MP : " + p.get_mp());
    }

    public int get_potion(){
        return this.potion;
    }

    public void add_potion(int num, int money){
        this.potion = this.potion + num;
        this.pokecoin = this.pokecoin - (money*num);
    }

    public void use_potion(Pokemon p){
        this.potion = this.potion - 1;
        p.heal_hp(50);
        System.out.println(" + 50 HP => Now HP : " + p.get_hp() + " ,MP : " + p.get_mp());
    }

    public int get_energy(){
        return this.energy;
    }

    public void add_energy(int num, int money){
        this.energy = this.energy + num;
        this.pokecoin = this.pokecoin - (money*num);
    }

    public void use_energy(Pokemon p){
        this.energy = this.energy - 1;
        p.heal_mp(50);
        System.out.println(" + 50 MP => Now HP : " + p.get_hp() + " ,MP : " + p.get_mp());
    }

    public int get_pokeball(){
        return this.pokeball;
    }    

    public void add_pokeball(int num, int money){
        this.pokeball = this.pokeball + num;
        this.pokecoin = this.pokecoin - (money*num);
    }

    public void set_pokeball(){
        this.pokeball = this.pokeball - 1;
    }

    public boolean use_pokeball(ArrayList<Pokemon> bag, Pokemon wildPokemon){
        this.pokeball = this.pokeball - 1;
        if(wildPokemon.get_hp() < 100){
            int random = (int)(Math.random()*2) + 1;
            if(random == 1){
                System.out.println("-------------------------------");
                System.out.println("Catched it Sucess !!");
                System.out.println("-------------------------------");
                bag.add(wildPokemon);
                return true;
            }
            else if(random == 2){
                System.out.println("-------------------------------");
                System.out.println("Catched it Fail !!");
                System.out.println("-------------------------------");
                return false;
            }
            
        }
        else if(wildPokemon.get_hp() < (wildPokemon.get_hp() / 2)){
            int random = (int)(Math.random()* 2) + 1;
            if(random == 1){
                System.out.println("-------------------------------");
                System.out.println("Catched it Sucess !!");
                System.out.println("-------------------------------");
                bag.add(wildPokemon);
                return true;
            }
            else if (random == 2){
                System.out.println("-------------------------------");
                System.out.println("Catched it Fail !!");
                System.out.println("-------------------------------");
                return false;
            }
        }
        else{
            System.out.println("-------------------------------");
            System.out.println("Catched it Fail !!");
            System.out.println("-------------------------------");
            return false;
        }
    return false;
    }
    
}