import java.util.*;

public class Randomskill{

    public static ArrayList<Skill> getskills(){
        ArrayList<Skill> Skills = new ArrayList<Skill>();

        int numberofskill = 20;

        for(int i = 0; i < numberofskill; i++){
            int num = (int)(Math.random()*8);
            int count = 0;
            
            if(num == 1){
                for(Skill s: Skills){
                    if(s.get_name().equals("thunderbolt"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Thunderbolt());
                }
            }
            else if(num == 2){
                for(Skill s: Skills){
                    if(s.get_name().equals("solarbeam"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Solarbeam());
                }
            }
            else if(num == 3){
                for(Skill s: Skills){
                    if(s.get_name().equals("hydropump"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Hydropump());
                }
            }
            else if(num == 4){
                for(Skill s: Skills){
                    if(s.get_name().equals("flamethrower"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Flamethrower());
                }
            }
            else if(num == 5){
                for(Skill s: Skills){
                    if(s.get_name().equals("earthquake"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Earthquake());
                }
            }
            else if(num == 6){
                for(Skill s: Skills){
                    if(s.get_name().equals("bodyslam"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Bodyslam());
                }
            }
            else if(num == 7){
                for(Skill s: Skills){
                    if(s.get_name().equals("blizzard"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Blizzard());
                }
            }
            else if(num == 8){
                for(Skill s: Skills){
                    if(s.get_name().equals("airslash"))
                        count++;
                }
                if(count == 0){
                    Skills.add(new Airslash());
                }
            }
        }
        return Skills;
    }
}