import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Useitem extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;
	private Pokemon myPokemon;
	private Pokemon wildPokemon;
	private int no;

	public Useitem(Trainer trainer, int no, Pokemon wildPokemon){
		super("Use Item");
        setVisible(true);
        setSize(490, 260);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
		this.myPokemon = bag.get(no);
		this.wildPokemon = wildPokemon;

		Container c = getContentPane();
		c.setLayout(null);

		JButton berry = new JButton();
		berry.setIcon(new ImageIcon("berry.png"));
		berry.setBounds(10, 10, 150, 150);

		JButton potion = new JButton();
        potion.setIcon(new ImageIcon("potion.png"));
        potion.setBounds(160, 10 , 150, 150);

        JButton energy = new JButton();
        energy.setIcon(new ImageIcon("energy.png"));
        energy.setBounds(310, 10 , 150, 150);

        JButton back = new JButton("<<<");
        back.setBounds(10, 170 , 450, 50);

        c.add(back);
        c.add(berry);
        c.add(potion);
        c.add(energy);

        back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MainBattle bt = new MainBattle(trainer, no, wildPokemon);
                setVisible(false);
            }
        });

        berry.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(item.get_berry() > 0){
            		item.use_berry(myPokemon);
            		MainBattle bt = new MainBattle(trainer, no, wildPokemon);
            		setVisible(false);
            	}
            	else{
            		JOptionPane.showMessageDialog(null, "Not Have Berry !!", "!!!!!", JOptionPane.PLAIN_MESSAGE);
            	}
			}
        });

        potion.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(item.get_potion() > 0){
            		item.use_potion(myPokemon);
            		MainBattle bt = new MainBattle(trainer, no, wildPokemon);
            		setVisible(false);		
            	}
            	else{
					JOptionPane.showMessageDialog(null, "Not Have Potion !!", "!!!!!", JOptionPane.PLAIN_MESSAGE);
            	}
			}
        });

        energy.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	if(item.get_energy() > 0){
            		item.use_energy(myPokemon);
            		MainBattle bt = new MainBattle(trainer, no, wildPokemon);
            		setVisible(false);
            	}
            	else{
            		JOptionPane.showMessageDialog(null, "Not Have Energy !!", "!!!!!", JOptionPane.PLAIN_MESSAGE);
            	}
			}
        });

	}
}