import java.util.*;

public abstract class Pokemon{
    protected String name = "";
    protected String element = "";
    protected int HP;
    protected int MP;
    protected int max_hp;
    protected int max_mp;
    protected int type;
    private ArrayList<Skill> skills = Randomskill.getskills();

    public Pokemon(){
    
    }

    public void new_name(String name){
        this.name = name;
    }

    public Pokemon(String name){
        this.name = name;
        this.HP = 0;
        this.element = element;
    }

    public Pokemon(String name, int max_hp, int max_mp, String element, int type){
        this.type = type;
        this.max_hp = max_hp;
        this.max_mp = max_mp;
        this.name = name;
        this.element = element;
        this.HP = (int)(Math.random() * max_hp);
        this.MP = max_mp;
        if(MP < 60){
            this.MP = 50;
        }
        if(HP < 10){
            this.HP = 10;
        }
    }

    public int get_type(){
        return type;
    }

    public int get_mp(){
        return MP;
    }

    public int get_maxhp(){
        return max_hp;
    }

    public int get_maxmp(){
        return max_mp;
    }

    public int get_hp(){
        return HP;
    }

    public String getName(){
        return name;
    }

    public ArrayList<Skill> getskill(){
        return skills;
    }

    public abstract void skill_1(Pokemon enemy, Pokemon myPokemon, Skill sk);
    public abstract void skill_2(Pokemon enemy, Pokemon myPokemon, Skill sk);
    public abstract void skill_3(Pokemon enemy, Pokemon myPokemon, Skill sk);
    public abstract void skill_4(Pokemon enemy, Pokemon myPokemon, Skill sk);

    public String getelement(){
        return element;
    }

    public boolean damage(int dm, int nama){
        if(HP == 0 || MP == 0){
            return false;
        }
        int current_HP = HP - dm;
        int current_MP = MP - nama;

        if(current_HP > 0){
            this.HP = current_HP;
        }
        else{
            this.HP = 0; 
        }

        if(current_MP > 0){
            this.MP = current_MP;
        }
        else{
            this.MP = 0; 
        }
        return true;
    }

    public String toString(){
        return name + " (" + element + ") ";
    }

    public boolean heal_hp(int heal){
        if(this.HP < 0){
            return false;
        }

        else if(this.HP > 0 && this.HP < max_hp){
            int afterheal = this.HP + heal;
            this.HP = afterheal;
            return true;
        }
        return true;
    }

    public boolean heal_mp(int heal){
        if(this.MP < 0){
            return false;
        }

        else if(this.MP > 0 && this.MP < max_mp){
            int afterheal = this.MP + heal;
            this.MP = afterheal;
            return true;
        }
        return true;
    }

    public void full_hp(){
        this.HP = max_hp;
        this.MP = max_mp;
    }

}