import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Change extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;
	private int no = 5;
    private Pokemon wildPokemon;

	public Change(Trainer trainer,int number, Pokemon wildPokemon){
		super("Battle");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
        this.wildPokemon = wildPokemon;

        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("dex.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

		ImageIcon qs = new ImageIcon("question.png");
        ImageIcon fd = new ImageIcon("dex_fuchidane.png");
        ImageIcon hk = new ImageIcon("dex_hitokage.png");
        ImageIcon kb = new ImageIcon("dex_kabigon.png");
        ImageIcon pi = new ImageIcon("dex_pikachu.png");
        ImageIcon sk = new ImageIcon("dex_sanikamea.png");

        JLabel pokemon = new JLabel(qs);
        pokemon.setBounds(160, 205, 210, 190);

        JLabel name = new JLabel("Name:");
        name.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        name.setBounds(600, 40, 300, 50);
        JLabel pname = new JLabel("-");
        pname.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        pname.setBounds(750, 40, 300, 50);
        JLabel hp = new JLabel("HP: ");
        hp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        hp.setBounds(600, 90, 300, 50);
        JLabel php = new JLabel("-");
        php.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        php.setBounds(700, 90, 300, 50);
        JLabel mp = new JLabel("MP:");
        mp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        mp.setBounds(900, 90, 300, 50);
        JLabel pmp = new JLabel("-");
        pmp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        pmp.setBounds(1000, 90, 300, 50);



        JLabel skill_1 = new JLabel("Skill I: ");
        skill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_1.setBounds(600, 150, 150, 40);
        JLabel pskill_1 = new JLabel(" - ");
        pskill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_1.setBounds(750, 150, 550, 40);
        //------------------------------
        JLabel damage_1 = new JLabel(" Damage: ");
        damage_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        damage_1.setBounds(650, 190, 550, 40);
        JLabel pdamage_1 = new JLabel(" - ");
        pdamage_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pdamage_1.setBounds(800, 190, 550, 40);
        //------------------------------
        JLabel mp_1 = new JLabel(" MP: ");
        mp_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        mp_1.setBounds(900, 190, 550, 40);
        JLabel pmp_1 = new JLabel(" - ");
        pmp_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pmp_1.setBounds(1000, 190, 550, 40);



        JLabel skill_2 = new JLabel("Skill II ");
        skill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_2.setBounds(600, 240, 150, 40);
        JLabel pskill_2 = new JLabel(" - ");
        pskill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_2.setBounds(750, 240, 550, 40);
        //------------------------------
        JLabel damage_2 = new JLabel(" Damage: ");
        damage_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        damage_2.setBounds(650, 290, 550, 40);
        JLabel pdamage_2 = new JLabel(" - ");
        pdamage_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pdamage_2.setBounds(800, 290, 550, 40);
        //------------------------------
        JLabel mp_2 = new JLabel(" MP: ");
        mp_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        mp_2.setBounds(900, 290, 550, 40);
        JLabel pmp_2 = new JLabel(" - ");
        pmp_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pmp_2.setBounds(1000, 290, 550, 40);



        JLabel skill_3 = new JLabel("Skill III: ");
        skill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_3.setBounds(600, 340, 150, 40);
        JLabel pskill_3 = new JLabel(" - ");
        pskill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_3.setBounds(750, 340, 550, 40);
        //------------------------------
        JLabel damage_3 = new JLabel(" Damage: ");
        damage_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        damage_3.setBounds(650, 390, 550, 40);
        JLabel pdamage_3 = new JLabel(" - ");
        pdamage_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pdamage_3.setBounds(800, 390, 550, 40);
        //------------------------------
        JLabel mp_3 = new JLabel(" MP: ");
        mp_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        mp_3.setBounds(900, 390, 550, 40);
        JLabel pmp_3 = new JLabel(" - ");
        pmp_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pmp_3.setBounds(1000, 390, 550, 40);



        JLabel skill_4 = new JLabel("Skill IV: ");
        skill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_4.setBounds(600, 440, 150, 40);
        JLabel pskill_4 = new JLabel(" - ");
        pskill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_4.setBounds(750, 440, 550, 40);
        //------------------------------
        JLabel damage_4 = new JLabel(" Damage: ");
        damage_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        damage_4.setBounds(650, 490, 550, 40);
        JLabel pdamage_4 = new JLabel(" - ");
        pdamage_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pdamage_4.setBounds(800, 490, 550, 40);
        //------------------------------
        JLabel mp_4 = new JLabel(" MP: ");
        mp_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        mp_4.setBounds(900, 490, 550, 40);
        JLabel pmp_4 = new JLabel(" - ");
        pmp_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pmp_4.setBounds(1000, 490, 550, 40);


        
        JButton battle = new JButton("Change");
        battle.setBounds(150, 542, 150, 70);
        battle.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 35));

        JButton back = new JButton("<<<");
        back.setBounds(10, 630, 80, 40);
        back.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

        JButton poke1 = new JButton("Slot 1");
        poke1.setBounds(537, 560, 130, 60);
        poke1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton poke2 = new JButton("Slot 2");
        poke2.setBounds(677, 560, 130, 60);
        poke2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton poke3 = new JButton("Slot 3");
        poke3.setBounds(817, 560, 130, 60);
        poke3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton poke4 = new JButton("Slot 4");
        poke4.setBounds(957, 560, 130, 60);
        poke4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton poke5 = new JButton("Slot 5");
        poke5.setBounds(1097, 560, 130, 60);
        poke5.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        background.add(back);
        background.add(battle);

        background.add(name);
        background.add(hp);
        background.add(mp);
        background.add(pname);
        background.add(php);
        background.add(pmp);

        background.add(skill_1);
        background.add(pskill_1);
		background.add(damage_1);
		background.add(mp_1);
		background.add(pdamage_1);
		background.add(pmp_1);

        background.add(skill_2);
        background.add(pskill_2);
		background.add(damage_2);
		background.add(mp_2);
		background.add(pdamage_2);
		background.add(pmp_2);

        background.add(skill_3);
        background.add(pskill_3);
		background.add(damage_3);
		background.add(mp_3);
		background.add(pdamage_3);
		background.add(pmp_3);

        background.add(skill_4);
        background.add(pskill_4);
		background.add(damage_4);
		background.add(mp_4);
		background.add(pdamage_4);
		background.add(pmp_4);


        background.add(poke1);
        background.add(poke2);
        background.add(poke3);
        background.add(poke4);
        background.add(poke5);
        background.add(pokemon);

        int sz = bag.size();
        // System.out.println(sz);

        back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Mainplay mp = new Mainplay(trainer);
                setVisible(false);
            }
        });

        battle.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(no != 5){
                	MainBattle mbt = new MainBattle(trainer, no, wildPokemon);
                	setVisible(false);
                }
                else{
                	JOptionPane.showMessageDialog(null, "Please Select Your Pokemon !!", "!!!!!", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });

		// System.out.println(bag.get(0).get_type());
        poke1.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	no = 0;
                if(sz >= 1){
                	Pokemon pk = bag.get(0);
                	ArrayList<Skill> skills = pk.getskill();
                	if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pmp.setText(Integer.toString(pk.get_mp()));

                	pskill_1.setText(skills.get(0).get_name());
                	pdamage_1.setText(Integer.toString(skills.get(0).get_damage()));
                	pmp_1.setText(Integer.toString(skills.get(0).get_Mp()));

                	pskill_2.setText(skills.get(1).get_name());
                	pdamage_2.setText(Integer.toString(skills.get(1).get_damage()));
                	pmp_2.setText(Integer.toString(skills.get(1).get_Mp()));

                	pskill_3.setText(skills.get(2).get_name());
                	pdamage_3.setText(Integer.toString(skills.get(2).get_damage()));
                	pmp_3.setText(Integer.toString(skills.get(2).get_Mp()));

                	pskill_4.setText(skills.get(3).get_name());
                	pdamage_4.setText(Integer.toString(skills.get(3).get_damage()));
                	pmp_4.setText(Integer.toString(skills.get(3).get_Mp()));

                }else{
                	pname.setText(" Empty Slot ");
                	pokemon.setIcon(qs);
                	php.setText(" - ");
                	pmp.setText(" - ");

                	pskill_1.setText(" - ");
                	pdamage_1.setText(" - ");
                	pmp_1.setText(" - ");

                	pskill_2.setText(" - ");
                	pdamage_2.setText(" - ");
                	pmp_2.setText((" - "));

                	pskill_3.setText(" - ");
                	pdamage_3.setText(" - ");
                	pmp_3.setText(" - ");

                	pskill_4.setText(" - ");
                	pdamage_4.setText(" - ");
                	pmp_4.setText(" - ");
                }
            }
        });

        poke2.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	no = 1;
                if(sz >= 2){
                	Pokemon pk = bag.get(0);
                	ArrayList<Skill> skills = pk.getskill();
                	if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pmp.setText(Integer.toString(pk.get_mp()));

                	pskill_1.setText(skills.get(0).get_name());
                	pdamage_1.setText(Integer.toString(skills.get(0).get_damage()));
                	pmp_1.setText(Integer.toString(skills.get(0).get_Mp()));

                	pskill_2.setText(skills.get(1).get_name());
                	pdamage_2.setText(Integer.toString(skills.get(1).get_damage()));
                	pmp_2.setText(Integer.toString(skills.get(1).get_Mp()));

                	pskill_3.setText(skills.get(2).get_name());
                	pdamage_3.setText(Integer.toString(skills.get(2).get_damage()));
                	pmp_3.setText(Integer.toString(skills.get(2).get_Mp()));

                	pskill_4.setText(skills.get(3).get_name());
                	pdamage_4.setText(Integer.toString(skills.get(3).get_damage()));
                	pmp_4.setText(Integer.toString(skills.get(3).get_Mp()));

                }else{
                	pname.setText(" Empty Slot ");
                	pokemon.setIcon(qs);
                	php.setText(" - ");
                	pmp.setText(" - ");

                	pskill_1.setText(" - ");
                	pdamage_1.setText(" - ");
                	pmp_1.setText(" - ");

                	pskill_2.setText(" - ");
                	pdamage_2.setText(" - ");
                	pmp_2.setText((" - "));

                	pskill_3.setText(" - ");
                	pdamage_3.setText(" - ");
                	pmp_3.setText(" - ");

                	pskill_4.setText(" - ");
                	pdamage_4.setText(" - ");
                	pmp_4.setText(" - ");
                }
            }
        });

        poke3.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	no = 2;
                if(sz >= 3){
                	Pokemon pk = bag.get(0);
                	ArrayList<Skill> skills = pk.getskill();
                	if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pmp.setText(Integer.toString(pk.get_mp()));

                	pskill_1.setText(skills.get(0).get_name());
                	pdamage_1.setText(Integer.toString(skills.get(0).get_damage()));
                	pmp_1.setText(Integer.toString(skills.get(0).get_Mp()));

                	pskill_2.setText(skills.get(1).get_name());
                	pdamage_2.setText(Integer.toString(skills.get(1).get_damage()));
                	pmp_2.setText(Integer.toString(skills.get(1).get_Mp()));

                	pskill_3.setText(skills.get(2).get_name());
                	pdamage_3.setText(Integer.toString(skills.get(2).get_damage()));
                	pmp_3.setText(Integer.toString(skills.get(2).get_Mp()));

                	pskill_4.setText(skills.get(3).get_name());
                	pdamage_4.setText(Integer.toString(skills.get(3).get_damage()));
                	pmp_4.setText(Integer.toString(skills.get(3).get_Mp()));

                }else{
                	pname.setText(" Empty Slot ");
                	pokemon.setIcon(qs);
                	php.setText(" - ");
                	pmp.setText(" - ");

                	pskill_1.setText(" - ");
                	pdamage_1.setText(" - ");
                	pmp_1.setText(" - ");

                	pskill_2.setText(" - ");
                	pdamage_2.setText(" - ");
                	pmp_2.setText((" - "));

                	pskill_3.setText(" - ");
                	pdamage_3.setText(" - ");
                	pmp_3.setText(" - ");

                	pskill_4.setText(" - ");
                	pdamage_4.setText(" - ");
                	pmp_4.setText(" - ");
                }
            }
        });

        poke4.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	no = 3;
                if(sz >= 4){
                	Pokemon pk = bag.get(0);
                	ArrayList<Skill> skills = pk.getskill();
                	if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pmp.setText(Integer.toString(pk.get_mp()));

                	pskill_1.setText(skills.get(0).get_name());
                	pdamage_1.setText(Integer.toString(skills.get(0).get_damage()));
                	pmp_1.setText(Integer.toString(skills.get(0).get_Mp()));

                	pskill_2.setText(skills.get(1).get_name());
                	pdamage_2.setText(Integer.toString(skills.get(1).get_damage()));
                	pmp_2.setText(Integer.toString(skills.get(1).get_Mp()));

                	pskill_3.setText(skills.get(2).get_name());
                	pdamage_3.setText(Integer.toString(skills.get(2).get_damage()));
                	pmp_3.setText(Integer.toString(skills.get(2).get_Mp()));

                	pskill_4.setText(skills.get(3).get_name());
                	pdamage_4.setText(Integer.toString(skills.get(3).get_damage()));
                	pmp_4.setText(Integer.toString(skills.get(3).get_Mp()));

                }else{
                	pname.setText(" Empty Slot ");
                	pokemon.setIcon(qs);
                	php.setText(" - ");
                	pmp.setText(" - ");

                	pskill_1.setText(" - ");
                	pdamage_1.setText(" - ");
                	pmp_1.setText(" - ");

                	pskill_2.setText(" - ");
                	pdamage_2.setText(" - ");
                	pmp_2.setText((" - "));

                	pskill_3.setText(" - ");
                	pdamage_3.setText(" - ");
                	pmp_3.setText(" - ");

                	pskill_4.setText(" - ");
                	pdamage_4.setText(" - ");
                	pmp_4.setText(" - ");
                }
            }
        });

        poke5.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	no = 4;
                if(sz >= 5){
                	Pokemon pk = bag.get(0);
                	ArrayList<Skill> skills = pk.getskill();
                	if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pmp.setText(Integer.toString(pk.get_mp()));

                	pskill_1.setText(skills.get(0).get_name());
                	pdamage_1.setText(Integer.toString(skills.get(0).get_damage()));
                	pmp_1.setText(Integer.toString(skills.get(0).get_Mp()));

                	pskill_2.setText(skills.get(1).get_name());
                	pdamage_2.setText(Integer.toString(skills.get(1).get_damage()));
                	pmp_2.setText(Integer.toString(skills.get(1).get_Mp()));

                	pskill_3.setText(skills.get(2).get_name());
                	pdamage_3.setText(Integer.toString(skills.get(2).get_damage()));
                	pmp_3.setText(Integer.toString(skills.get(2).get_Mp()));

                	pskill_4.setText(skills.get(3).get_name());
                	pdamage_4.setText(Integer.toString(skills.get(3).get_damage()));
                	pmp_4.setText(Integer.toString(skills.get(3).get_Mp()));

                }else{
                	pname.setText(" Empty Slot ");
                	pokemon.setIcon(qs);
                	php.setText(" - ");
                	pmp.setText(" - ");

                	pskill_1.setText(" - ");
                	pdamage_1.setText(" - ");
                	pmp_1.setText(" - ");

                	pskill_2.setText(" - ");
                	pdamage_2.setText(" - ");
                	pmp_2.setText((" - "));

                	pskill_3.setText(" - ");
                	pdamage_3.setText(" - ");
                	pmp_3.setText(" - ");

                	pskill_4.setText(" - ");
                	pdamage_4.setText(" - ");
                	pmp_4.setText(" - ");
                }
            }
        });

	}

}