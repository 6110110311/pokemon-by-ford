import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Mainplay extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;

	public Mainplay(Trainer trainer){
        super("Pokemon Game");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();

        // System.out.println("" + item.get_coin() + "");

        Container c = getContentPane();
        c.setLayout(new GridLayout(2,2,0,0));

        JButton battle = new JButton();
        JButton inventory = new JButton();
        JButton shop = new JButton();
        JButton quit = new JButton();

        battle.setIcon(new ImageIcon("battle.png"));
        inventory.setIcon(new ImageIcon("inventory.png"));
        shop.setIcon(new ImageIcon("shop.png"));
        quit.setIcon(new ImageIcon("quit.png"));

        c.add(battle);
        c.add(inventory);
        c.add(shop);
        c.add(quit);

        battle.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        System.out.println("Battle");
                        setVisible(false);
                        Battlegui bt = new Battlegui(trainer);
                }
        });

        inventory.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        System.out.println("Inventory");
                        setVisible(false);
                        Inventory inv = new Inventory(trainer);
                }
        });

        shop.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        System.out.println("Shop");
                        setVisible(false);
                        Shop shop = new Shop(trainer);
                }
        });

        quit.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                        System.out.println("Quit");
                        System.exit(0);
                }
        });

	}

}