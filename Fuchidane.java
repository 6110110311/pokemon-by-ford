public class Fuchidane extends Pokemon{
    // public String elementPokemon = "grass";        

    public Fuchidane(){
        
    }

    public Fuchidane(String name){
        super(name, 200, 200,"grass", 1);
    }
    
    public void skill_1(Pokemon enemy, Pokemon myPokemon, Skill sk){
        // System.out.println("\n" +name + " Attack " + enemy.getName());
        enemy.damage(sk.get_damage(), 0);
        myPokemon.damage(0, sk.get_Mp());
        // System.out.println(name + " HP: " + super.get_hp() + " MP: " + super.get_mp());
        // System.out.println(enemy.getName() + " HP : " + enemy.get_hp());
        // System.out.println("---------------------------------------------");
    }

    public void skill_2(Pokemon enemy, Pokemon myPokemon, Skill sk){
        System.out.println("\n" +name + " Attack " + enemy.getName());
        enemy.damage(sk.get_damage(), 0);
        myPokemon.damage(0, sk.get_Mp());
        // System.out.println(name + " HP: " + super.get_hp() + " MP: " + super.get_mp());
        // System.out.println(enemy.getName() + " HP : " + enemy.get_hp());
        // System.out.println("---------------------------------------------");
    }

    public void skill_3(Pokemon enemy, Pokemon myPokemon, Skill sk){
        // System.out.println("\n" +name + " Attack " + enemy.getName());
        enemy.damage(sk.get_damage(), 0);
        myPokemon.damage(0, sk.get_Mp());
    //     System.out.println(name + " HP: " + super.get_hp() + " MP: " + super.get_mp());
    //     System.out.println(enemy.getName() + " HP : " + enemy.get_hp());
    //     System.out.println("---------------------------------------------");
    }

    public void skill_4(Pokemon enemy, Pokemon myPokemon, Skill sk){
        // System.out.println("\n" +name + " Attack " + enemy.getName());
        enemy.damage(sk.get_damage(), 0);
        myPokemon.damage(0, sk.get_Mp());
        // System.out.println(name + " HP: " + super.get_hp() + " MP: " + super.get_mp());
        // System.out.println(enemy.getName() + " HP : " + enemy.get_hp());
        // System.out.println("---------------------------------------------");
    }

}