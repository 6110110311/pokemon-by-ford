import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Lose extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;

	public Lose(Trainer trainer){
		super("LOSE");
        setVisible(true);
        setSize(840, 630);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("lose.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

        JButton next = new JButton("<< OK >>");
        next.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        next.setBounds(285,500,250,60);

        background.add(next);


        next.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	Mainplay mp = new Mainplay(trainer);
            	setVisible(false);
            }
        });
	}
}