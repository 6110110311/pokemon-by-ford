import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Choose_wildpokemon extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;
	private int number;
	private int no = 5;
	private int i = 0;

	public Choose_wildpokemon(Trainer trainer, int no){
		super("WildPokedex");
        setVisible(true);
        setSize(1280, 720);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.trainer = trainer;
        this.bag = trainer.get_Bag();
        this.item = trainer.get_item();
        this.no = no;

        
        JButton button[] = {new JButton("1"), new JButton("2"), new JButton("3"), new JButton("4"), new JButton("5"), new JButton("6"), new JButton("7"), new JButton("8")};
		ArrayList<Pokemon> pokemons = PokemonRandom.getPokemons(8);
		// Print printPokemon = new Print();
		// printPokemon.play(pokemons);

		Container c = getContentPane();
		JLabel background;
        ImageIcon bg = new ImageIcon("wild.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

		System.out.println(pokemons.size());
		int num = pokemons.size();
		int row_1 = 125, y_1 = 540, row_2 = 50, y_2 = 150;

        for(i = 0; i < num; i++){
        	button[i].setBounds(row_1, y_1, 50, 50);
        	row_1 += 50;
        	background.add(button[i]);

        }

        ImageIcon qs = new ImageIcon("question.png");
        ImageIcon fd = new ImageIcon("dex_fuchidane.png");
        ImageIcon hk = new ImageIcon("dex_hitokage.png");
        ImageIcon kb = new ImageIcon("dex_kabigon.png");
        ImageIcon pi = new ImageIcon("dex_pikachu.png");
        ImageIcon sk = new ImageIcon("dex_sanikamea.png");


        JLabel pokemon = new JLabel(qs);
        pokemon.setBounds(225, 200, 210, 190);

        JLabel name = new JLabel("Name:");
        name.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        name.setBounds(650, 100, 300, 50);
        JLabel pname = new JLabel("-");
        pname.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        pname.setBounds(800, 100, 300, 50);
        JLabel hp = new JLabel("HP: ");
        hp.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        hp.setBounds(700, 150, 300, 50);
        JLabel php = new JLabel("-");
        php.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 50));
        php.setBounds(800, 150, 300, 50);



        JLabel skill_1 = new JLabel("Skill I: ");
        skill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_1.setBounds(650, 250, 150, 40);
        JLabel pskill_1 = new JLabel(" - ");
        pskill_1.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_1.setBounds(800, 250, 550, 40);

        JLabel skill_2 = new JLabel("Skill II ");
        skill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_2.setBounds(650, 300, 150, 40);
        JLabel pskill_2 = new JLabel(" - ");
        pskill_2.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_2.setBounds(800, 300, 550, 40);

        JLabel skill_3 = new JLabel("Skill III: ");
        skill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_3.setBounds(650, 350, 150, 40);
        JLabel pskill_3 = new JLabel(" - ");
        pskill_3.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_3.setBounds(800, 350, 550, 40);

        JLabel skill_4 = new JLabel("Skill IV: ");
        skill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        skill_4.setBounds(650, 400, 150, 40);
        JLabel pskill_4 = new JLabel(" - ");
        pskill_4.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 40));
        pskill_4.setBounds(800, 400, 550, 40);

		

        JButton back = new JButton("<<<");
        back.setBounds(10, 630, 80, 40);
        back.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

        JButton next = new JButton(">>>");
        next.setBounds(1170, 630, 80, 40);
        next.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 15));

        

		background.add(pokemon);

        background.add(next);
        background.add(back);

        background.add(name);
        background.add(hp);
        background.add(pname);
        background.add(php);

        background.add(skill_1);
        background.add(pskill_1);

        background.add(skill_2);
        background.add(pskill_2);

        background.add(skill_3);
        background.add(pskill_3);

        background.add(skill_4);
        background.add(pskill_4);

        back.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Battlegui bt = new Battlegui(trainer);
                setVisible(false);
            }
        });

        next.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MainBattle mbt = new MainBattle(trainer, no, pokemons.get(number));
                setVisible(false);
            }
        });


        button[0].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 0;
            	Pokemon pk = pokemons.get(0);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[1].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 1;
            	Pokemon pk = pokemons.get(1);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[2].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 2;
            	Pokemon pk = pokemons.get(2);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[3].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 3;
            	Pokemon pk = pokemons.get(3);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[4].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 4;
            	Pokemon pk = pokemons.get(4);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[5].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 5;
            	Pokemon pk = pokemons.get(5);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[6].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 6;
            	Pokemon pk = pokemons.get(6);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
        button[7].addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	number = 7;
            	Pokemon pk = pokemons.get(7);
            	ArrayList<Skill> skills = pk.getskill();
            		if(pk.get_type() == 1){
                		pokemon.setIcon(fd);
                	}
                	else if(pk.get_type() == 2){
                		pokemon.setIcon(hk);
                	}
                	else if(pk.get_type() == 3){
                		pokemon.setIcon(kb);
                	}
                	else if(pk.get_type() == 4){
                		pokemon.setIcon(pi);
                	}
                	else if(pk.get_type() == 5){
                		pokemon.setIcon(sk);
                	}

                	pname.setText(pk.getName());
                	php.setText(Integer.toString(pk.get_hp()));
                	pskill_1.setText(skills.get(0).get_name());
                	pskill_2.setText(skills.get(1).get_name());
                	pskill_3.setText(skills.get(2).get_name());
                	pskill_4.setText(skills.get(3).get_name());
            }
        });
	}
} 