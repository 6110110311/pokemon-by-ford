import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Win extends JFrame{
	private Trainer trainer;
	private ArrayList<Pokemon> bag;
	private Item item;

	public Win(Trainer trainer){
		super("WIN");
        setVisible(true);
        setSize(840, 630);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("win.png");
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);

        ImageIcon berry = new ImageIcon("berry.png");
        ImageIcon co = new ImageIcon("pkcoin.png");

        JLabel reward = new JLabel(berry);
        JLabel coi = new JLabel(co);

        reward.setBounds(280, 230, 90, 90);
        coi.setBounds(450, 240, 90, 90);

        int rm = (int)(Math.random() * 2) + 1;
        int coin = (int)(Math.random() * 100);
        
        trainer.get_item().add_berry(rm, 0);
        trainer.get_item().add_coin(coin);
            // System.out.println("You get " + coin + " Pokecoin");

        JLabel preward = new JLabel("" + rm + "");
        JLabel pcoi = new JLabel("" + coin + "");

        preward.setBounds(330,320,250,60);
        pcoi.setBounds(490,320,250,60);
        preward.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        pcoi.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));

        JButton next = new JButton("<< GET IT >>");
        next.setFont(new Font("Layiji MaHaNiYom V1.5 OT", Font.BOLD, 30));
        next.setBounds(285,500,250,60);

        background.add(next);
        background.add(reward);
        background.add(coi);
        background.add(pcoi);
        background.add(preward);


        next.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	Mainplay mp = new Mainplay(trainer);
            	setVisible(false);
            }
        });
	}
}